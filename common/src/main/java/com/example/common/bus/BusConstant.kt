package com.example.common.bus

import android.view.WindowManager


class BusConstant {
    companion object {
        val  LOADING = "loading"
        val  ERROR = "error"
        val  TOKENEXCEPTION = "tokenexception"
    }
}