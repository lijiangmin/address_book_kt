package com.example.common.module

import android.util.Log
import com.example.common.BuildConfig
import com.example.common.config.LocalCookieJar
import com.orhanobut.logger.Logger
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.internal.managers.ApplicationComponentManager
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object HTTPModule {

    private const val TAG = "RetrofitManager"


//    val BASEURLLOCAL:String = "http://10.77.5.201:8082/sp/"
    val BASEURLLOCAL:String = "http://36.134.81.164:22228/sp/"

    val BASEURL:String = "http://36.134.81.164:22228/sp/"

    @Singleton
    @Provides
    fun provideRetrofit(okHttp: OkHttpClient): Retrofit {
        if(BuildConfig.DEBUG){
            return Retrofit.Builder()
                .baseUrl(BASEURLLOCAL)
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }else{
            return Retrofit.Builder()
                .baseUrl(BASEURL)
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .callTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .followRedirects(false)
            .cookieJar(LocalCookieJar())
            .addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Logger.d( "log: $message")
                }

            }).setLevel(HttpLoggingInterceptor.Level.BODY)).build()
    }
}