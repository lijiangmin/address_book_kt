package com.example.common.base

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.bus.BusConstant
import com.example.common.bus.LiveDataBus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class BaseViewModel: ViewModel() {

//    protected var context: Context? = null

    val loadingLiveData = SingleLiveData<Boolean>()

    val errorLiveData = SingleLiveData<Throwable>()

    /*open fun addContext(context: FragmentActivity?) {
        this.context = context
    }

    open fun addContext(context: Context?) {
        this.context = context
    }*/

    /**
     * @deprecated
     */
    fun launch(
        block: suspend () -> Unit,
        error: suspend (Throwable) -> Unit,
        complete: suspend () -> Unit
    ) {
        loadingLiveData.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                block()
            } catch (e: Exception) {
                error(e)
            } finally {
                loadingLiveData.postValue(false)
                complete()
            }
        }
    }

    /**
     * 弹Toast
     */
    private fun showToast(msg: String) {
        LiveDataBus.with(BusConstant.ERROR, String::class.java).postValue(msg)
    }

    /**
     * show 加载中
     */
    fun showLoading() {
        if(LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).value==false){
            LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(true)
        }
    }

    /**
     * dismiss loading dialog
     */
    fun dismissLoading() {
        if(LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).value==true){
            LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(false)
        }
    }

}