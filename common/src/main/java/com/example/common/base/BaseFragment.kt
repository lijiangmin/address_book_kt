package com.example.common.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.carman.kotlin.coroutine.base.BaseActivity
import com.example.common.weight.LoadingDialog

abstract class BaseFragment <VB : ViewDataBinding>() : Fragment(), BaseBinding<VB> {
    protected lateinit var mBinding: VB
        private set


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = getViewBinding(inflater, container)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.initBinding()
    }

    override fun onDestroy() {
        super.onDestroy()
        //避免内存泄露
//        if (::mBinding.isInitialized) {
//            mBinding
//        }
    }

    open fun onBackPressed(): Boolean {
        return false
    }
    override fun onDestroyView() {
        super.onDestroyView()
        if (::mBinding.isInitialized) {
            mBinding.unbind()
        }
    }

    fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }


}
