package com.example.common.base

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.ExpandableListView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager

class BaseDialogFragment : DialogFragment() {

    private var mRootView: View? = null
    private var mDialog: Dialog? = null
    private var mContext: Context? = null
    private var widthPercentSize = 0f
    private var heightPercentSize = 0f
    private var layoutResID = 0
    private var holder: BaseDialogHolder? = null
    private var viewIds: IntArray? = null
    private var mTheme = 0
    private var cancel = false
    private var touchOutside = false
    private val simpleName : String = "BaseDialogFragment"
    private var animations : Int =0

    companion object {
        fun newInstance(): BaseDialogFragment {
            return BaseDialogFragment()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDialog = dialog
        if (mDialog != null) {
            mDialog?.setOnDismissListener(null)
            mDialog?.setOnCancelListener(null)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (mTheme != 0) {
            setStyle(STYLE_NORMAL, mTheme)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mRootView == null) {
            mRootView = inflater.inflate(layoutResID, container, false)
            setDialogSize(widthPercentSize, heightPercentSize)
        }
        return mRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDialog = dialog

        holder = BaseDialogHolder.create(mRootView, this)

        if (viewConvertListener != null) {
            viewConvertListener!!.convertView(holder, this)
        }
        if (holder != null && viewIds != null && viewIds!!.size > 0) {
            holder!!.addChildClickListener(viewIds!!)
        }
        if (mDialog != null) {
            val window = mDialog?.window
            window?.setWindowAnimations(animations)
            mDialog?.setCanceledOnTouchOutside(touchOutside)
            mDialog?.setOnKeyListener { _: DialogInterface?, _: Int, _: KeyEvent? -> cancel }
        }
    }

    /**
     * 其他区域是否点击消失
     * @param touchOutside
     * @return
     */
    fun setCanceledOnTouchOutside(touchOutside: Boolean): BaseDialogFragment {
        this.touchOutside = touchOutside
        return this
    }

    /**
     * 屏蔽返回键
     * @param cancel
     * @return
     */
    fun setCanceledOnTouch(cancel: Boolean): BaseDialogFragment {
        this.cancel = cancel
        return this
    }

    /**
     * 设置Style
     * @param theme
     * @return
     */
    fun setStyle(theme: Int): BaseDialogFragment {
        this.mTheme = theme
        return this
    }

    /**
     * 设置dialog动画
     */
    fun setWindowAnimations(animations: Int): BaseDialogFragment {
        this.animations = animations
        return this
    }

    /**
     * 设置布局
     * @param layoutResID
     * @return
     */
    fun setContentView(@LayoutRes layoutResID: Int): BaseDialogFragment {
        this.layoutResID = layoutResID
        return this
    }

    /**
     * 设置显示的宽
     * @param widthPercentSize
     * @return
     */
    fun setWidthPercentSize(widthPercentSize: Float): BaseDialogFragment {
        this.widthPercentSize = widthPercentSize
        return this
    }

    /**
     * 显示高
     * @param heightPercentSize
     * @return
     */
    fun setHeightPercentSize(heightPercentSize: Float): BaseDialogFragment {
        this.heightPercentSize = heightPercentSize
        return this
    }


    private fun setFragmentManager(manager: FragmentManager?) {
        try {
            manager?.beginTransaction()?.remove(this)?.commitAllowingStateLoss()
            manager?.let { super.show(it, simpleName) }
        }catch (e:Exception){

        }
    }

    fun show(obj: Any?): BaseDialogFragment {
        if (obj != null) {
            when (obj) {
                is FragmentActivity -> {
                    setFragmentManager(obj.supportFragmentManager)
                }
                is Fragment -> {
                    setFragmentManager(obj.fragmentManager)
                }
                else -> {
                    throw IllegalArgumentException("not is FragmentManager")
                }
            }
        }
        return this
    }

    /**
     * 设置显示屏幕大小
     */
    private fun setDialogSize(widthPercent: Float, heightPercent: Float) {
        val dm = DisplayMetrics()
        val activity = activity
        val dialog = dialog
        if (activity != null && dialog != null) {
            activity.windowManager.defaultDisplay.getMetrics(dm)
            val window = dialog.window
            if (window != null) {
                window.decorView.setPadding(0, 0, 0, 0)
                val lp = window.attributes
                lp.width = if (widthPercent == 0f) WindowManager.LayoutParams.WRAP_CONTENT else
                    if (widthPercent == 1f) WindowManager.LayoutParams.MATCH_PARENT else
                        (dm.widthPixels * widthPercent).toInt()
                lp.height = if (heightPercent == 0f) WindowManager.LayoutParams.WRAP_CONTENT else
                    if (heightPercent == 1f) WindowManager.LayoutParams.MATCH_PARENT else
                        (dm.heightPixels * heightPercent).toInt()
                window.attributes = lp
                window.setBackgroundDrawable(ColorDrawable())
            }
        }
    }

    /**
     * 可以暴露所有的view
     */
    interface ConvertViewListener {
        fun convertView(holder: BaseDialogHolder?, dialog: BaseDialogFragment?)
    }

    private var viewConvertListener: ConvertViewListener? = null
    fun setConvertViewListener(l: ConvertViewListener?): BaseDialogFragment {
        viewConvertListener = l
        return this
    }

    var onChildClickListener: OnChildClickListener? = null
        private set

    /**
     * 设置子View Listener
     */
    fun setOnChildClickListener(l: OnChildClickListener?, @IdRes vararg viewIds: Int): BaseDialogFragment {
        onChildClickListener = l
        addIdRes(*viewIds)
        return this
    }

    /**
     * 设置子View Listener
     */
    fun setOnChildClickListener(l: OnChildClickListener?): BaseDialogFragment {
        onChildClickListener = l
        return this
    }

    /**
     * 子view添加
     * @param viewIds
     * @return
     */
    fun addIdRes(@IdRes vararg viewIds: Int): BaseDialogFragment {
        this.viewIds = viewIds
        return this
    }

}