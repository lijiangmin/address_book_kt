package com.example.common.base

import android.view.View

interface OnChildClickListener {
    fun onChildViewClick(dialog: BaseDialogFragment?, v: View?)
}