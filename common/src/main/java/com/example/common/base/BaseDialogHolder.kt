package com.example.common.base

import android.util.SparseArray
import android.view.View
import androidx.annotation.IdRes

class BaseDialogHolder(private var convertView: View?, dialogFragment: BaseDialogFragment?) {

    private val views: SparseArray<View?> = SparseArray()
    private val childClickViewIds: LinkedHashSet<Int> = LinkedHashSet()
    private val dialogFragment: BaseDialogFragment? = dialogFragment

    fun <T : View?> findViewById(@IdRes viewId: Int): T? {
        var view = views[viewId]
        if (view == null) {
            view = convertView?.findViewById(viewId)
            views.put(viewId, view)
        }
        return view as T?
    }

    /**
     * 添加子view Listener
     * @param viewIds
     * @return
     */
    fun addChildClickListener(viewIds: IntArray): BaseDialogHolder {
        for (viewId in viewIds) {
            childClickViewIds.add(viewId)
            val view = findViewById<View>(viewId)
            if (view != null) {
                if (!view.isClickable) {
                    view.isClickable = true
                }
                view.setOnClickListener(View.OnClickListener { v: View? ->
                    if (dialogFragment?.onChildClickListener != null) {
                        dialogFragment.onChildClickListener!!.onChildViewClick(dialogFragment, v)
                    }
                })
            }
        }
        return this
    }

    companion object {
        fun create(view: View?, dialogFragment: BaseDialogFragment?): BaseDialogHolder {
            return BaseDialogHolder(view, dialogFragment)
        }
    }

}

