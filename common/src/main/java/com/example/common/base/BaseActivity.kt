package com.carman.kotlin.coroutine.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.common.base.*
import com.example.common.bus.BusConstant
import com.example.common.bus.LiveDataBus
import com.example.common.weight.LoadingDialog


/**
 *
 *@author carman
 * @time 2021-4-16 13:25
 */
abstract class BaseActivity<VB : ViewDataBinding>() : AppCompatActivity(), BaseBinding<VB> {


    protected val mBinding: VB by lazy(mode = LazyThreadSafetyMode.NONE) {
        getViewBinding(layoutInflater)
    }

    private lateinit var mLoadingDialog: LoadingDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mBinding.root)
        mBinding.initBinding()
        mLoadingDialog = LoadingDialog(this, false)

        //消费者订阅消息
        LiveDataBus.with(BusConstant.LOADING, Boolean::class.java)
            .observe(this, {
                if (it) {
                    showLoading();
                } else {
                    dismissLoading();
                }
            })

//        getViewModel().loadingLiveData.observe(this, {
//            if (it) {
//                showLoading();
//            } else {
//                dismissLoading();
//            }
//        });

        //消费者订阅消息
        LiveDataBus.with(BusConstant.ERROR, String::class.java)
            .observe(this, {
                showToast(it)
            })
        initObserve()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    abstract fun initObserve()



    override fun onBackPressed() {
        super.onBackPressed()
        val fragments: List<Fragment> = supportFragmentManager.fragments
        if (!fragments.isNullOrEmpty()) {
            for (fragment in fragments) {
                if (fragment is BaseFragment<*>) {
                    if (fragment.onBackPressed()) {
                        return
                    }
                }
            }
        }
        super.onBackPressed()
    }

    /**
     * show 加载中
     */
    fun showLoading() {
        mLoadingDialog.showDialog(this, false)
    }

    /**
     * dismiss loading dialog
     */
    fun dismissLoading() {
        mLoadingDialog.dismissDialog()
    }

    fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun commonNetworkErrorListener(error: Throwable) {
        error.message?.let { showToast(it) }
    }

}
