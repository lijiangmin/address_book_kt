package com.example.address_book_kt.module

import org.json.JSONObject

data class AddressBook(
    val id:Int,
    var mobiles:MutableMap<String,Mobile>,
)
data class Mobile(
    var id:String,
    var label:String,
    var mobile:String
)