package com.example.address_book_kt.module

import com.example.address_book_kt.ui.address.FirstApi
import com.example.address_book_kt.ui.login.repo.LoginApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MyModule {

    @Provides
    @Singleton
    fun provideLoginApi(retrofit: Retrofit): LoginApi = retrofit.create(LoginApi::class.java)

    @Provides
    @Singleton
    fun provideFirstApi(retrofit: Retrofit): FirstApi = retrofit.create(FirstApi::class.java)

}