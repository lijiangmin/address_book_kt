package com.example.address_book_kt.ui.login.repo

import com.example.address_book_kt.data.model.AppVersion
import com.example.common.base.SingleLiveData
import com.example.common.http.BaseRepository
import com.fuusy.common.network.net.StateLiveData
import javax.inject.Inject

class LoginRepo @Inject constructor( private val service:LoginApi) : BaseRepository() {

    suspend fun login(userName: String, password: String, stateLiveData: StateLiveData<String>) {
        executeResp({ service.login(userName, password) }, stateLiveData)
    }

    suspend fun register(
        userName: String,
        password: String,
        rePassword: String,
        stateLiveData: StateLiveData<String>,loadingLiveData :SingleLiveData<Boolean>
    ) {
        executeResp({ service.register(userName, password, rePassword) }, stateLiveData)
    }

    suspend fun checkVersion(code: String, name: String, stateLiveData: StateLiveData<AppVersion>) {
        executeResp({ service.checkVersion(code, name) }, stateLiveData)
    }
}