package com.example.address_book_kt.ui.address

import android.content.ContentProviderOperation
import android.content.ContentValues
import android.content.Context
import android.provider.ContactsContract
import androidx.core.database.getStringOrNull
import androidx.lifecycle.MutableLiveData
import com.example.address_book_kt.MyApplication
import com.example.address_book_kt.data.model.ImportPhone
import com.example.address_book_kt.module.AddressBook
import com.example.address_book_kt.module.Mobile
import com.example.common.bus.BusConstant
import com.example.common.bus.LiveDataBus
import com.example.common.http.BaseRepository
import com.example.common.util.ToastUtil
import com.fuusy.common.network.net.StateLiveData
import com.orhanobut.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import org.json.JSONObject
import javax.inject.Inject

class FirstRepository @Inject constructor(private val service: FirstApi) : BaseRepository() {

    suspend fun searchData(myPhone:String,
        addressBooks: MutableMap<String,AddressBook>,
        stateLiveData: StateLiveData<MutableList<ImportPhone>>
    ) {
        if (addressBooks.size == 0) {
            LiveDataBus.with(BusConstant.ERROR, String::class.java).postValue("本地通讯录无数据");
            return
        }
        var data = mutableListOf<AddressBook>()
//        网络查询获取短号
//        var longPhones = addressBooks.map { it.value }
        var longPhones:MutableList<String> = mutableListOf<String>();
        var keys = addressBooks.keys;
        for (k in keys) {
            var mobiles = addressBooks.get(k)?.mobiles;
            var mk = mobiles?.keys;
            if (mk != null) {
                for(mk in mk){
                    if(!longPhones.contains(mobiles?.get(mk)?.mobile)&& !(mobiles?.get(mk)?.label?.contains("短号") )!!){
                        longPhones.add(mobiles.get(mk)!!.mobile)
                    }
                }
            }
        }
        var longPhonesStr =longPhones.toString();

//        if (MyApplication.token == null) {
//            LiveDataBus.with(BusConstant.ERROR, String::class.java).postValue("无登录信息，请退出重新登录");
//            return
//        }

        executeResp({ service.searchShort(myPhone, longPhonesStr) }, stateLiveData)

    }

    suspend fun updateTXL(
        addressBooks: MutableMap<String,AddressBook>,
        searchData: StateLiveData<MutableList<ImportPhone>>
        ,result: MutableLiveData<String>
    ) {

        var newCount:Int = 0;
        var updateCount:Int = 0;
        var all = 0;

            if (searchData.value == null || searchData.value?.data == null) {
                LiveDataBus.with(BusConstant.ERROR, String::class.java).postValue("无匹配号码");
            }else{
                all = searchData.value!!.data!!.size
                flow {
                    val ops = ArrayList<ContentProviderOperation>()
                    for (import in searchData.value!!.data!!) {
                        for (ak in addressBooks.keys) {
                            var addressbook = addressBooks.get(ak);
                            var mobiles = addressbook?.mobiles;
                            var ml = mobiles?.let { hasLong(it,import) };
                            if(ml!=null&&ml>0){
                                //能匹配到长号
                                var ms = mobiles?.let { hasShort(it,import) };
                                var msl = mobiles?.let { hasShortLabel(it,import) };
                                if(ms!=null){
                                    //能匹配到短号
                                    //无事发生
                                }else if(msl!=null){
                                    //存在短号字段，修改
                                    //修改
                                    ops.add(
                                        ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                                            .withSelection(
                                                ContactsContract.Contacts.Data._ID.toString() + "=?",
                                                arrayOf(msl.id)
                                            )
                                            .withValue(
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                import.shortPhone
                                            )
                                            .build()
                                    )
                                    updateCount++
                                }else{
                                    //新增
                                    val values = ContentValues()
                                    values.put(ContactsContract.Contacts.Data.RAW_CONTACT_ID, ak);
                                    values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, import.shortPhone)
                                    values.put(
                                        ContactsContract.Contacts.Data.MIMETYPE,
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                                    );
                                    values.put(
                                        ContactsContract.CommonDataKinds.Phone.TYPE,
                                        ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM
                                    )
                                    values.put(ContactsContract.CommonDataKinds.Phone.LABEL, "短号")
                                    ops.add(
                                        ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValues(values).build()
                                    );
                                    newCount++
                                }

                            }


                            /*if(addressBook.mobile.replace(" ","")==import.longPhone){

                                var jsonObject = addressBook.map;
                                if (addressBook.lavel.startsWith("短号")&&addressBook.) {
                                    //修改
                                    ops.add(
                                        ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                                            .withSelection(
                                                ContactsContract.Contacts.Data._ID.toString() + "=?",
                                                arrayOf(jsonObject.getString("短号_id"))
                                            )
                                            .withValue(
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                import.shortPhone
                                            )
                                            .build()
                                    )
                                    updateCount++
                                } else {
                                    //新增
                                    val values = ContentValues()
                                    values.put(ContactsContract.Contacts.Data.RAW_CONTACT_ID, addressBook.id);
                                    values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, import.shortPhone)
                                    values.put(
                                        ContactsContract.Contacts.Data.MIMETYPE,
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                                    );
                                    values.put(
                                        ContactsContract.CommonDataKinds.Phone.TYPE,
                                        ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM
                                    )
                                    values.put(ContactsContract.CommonDataKinds.Phone.LABEL, "短号")
                                    ops.add(
                                        ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValues(values).build()
                                    );
                                    newCount++
                                }
                            }*/

                        }
                    }
                    MyApplication.context.contentResolver?.applyBatch(ContactsContract.AUTHORITY, ops);
                    emit(addressBooks)
                }
                    .flowOn(Dispatchers.IO)
                    .onStart {
                        LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(true)
                    }
                    .onEmpty {
                        Logger.d("executeReqWithFlow:onEmpty")
                    }
                    .catch { exception ->
                        run {
                            ToastUtil.show("更新通讯录出错啦，请联系管理员")
                        }
                    }
                    .onCompletion {
                        LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(false)
                        ToastUtil.show("更新完成，查询号码${all}个，新增${newCount}个号码，更新${updateCount}个号码")
                        result.postValue("更新完成，查询号码${all}个，新增${newCount}个号码，更新${updateCount}个号码")
                    }.collect {
                        Logger.d("executeReqWithFlow: collect")
                    }
            }
    }

    suspend fun hasLong(mobiles:MutableMap<String,Mobile>,import:ImportPhone):  Int {
        var i = 0;
        for(mobile in mobiles){
            if(mobile.value.mobile.equals(import.longPhone)){
                i++;
            }
        }
        return i;
    }


    suspend fun hasShort(mobiles:MutableMap<String,Mobile>,import:ImportPhone):  Mobile? {
        for(mobile in mobiles){
            if(mobile.value.mobile.equals(import.shortPhone)){
                return mobile.value;
            }
        }
        return null;
    }
    suspend fun hasShortLabel(mobiles:MutableMap<String,Mobile>,import:ImportPhone):  Mobile? {
        for(mobile in mobiles){
            if(mobile.value.label.equals("短号")){
                return mobile.value;
            }
        }
        return null;
    }
    suspend fun getTXL(context: Context): MutableMap<String,AddressBook> {

        var contactData: MutableMap<String,AddressBook> = getContactInfo(context);
        return contactData;

//        var data = mutableListOf<AddressBook>()
//        flow {
//            val ops = ArrayList<ContentProviderOperation>()
//            var contactData: MutableMap<String,AddressBook> = getContactInfo(context);
//
//            for (i in 0..(contactData.names().length() - 1)) {
//                var jsonObject = contactData.getJSONObject(contactData.names().get(i).toString());
//                var addressBook: AddressBook = AddressBook(
//                    id = contactData.names().get(i).toString(),
//                    emptyMap<String,Mobile>(),
//                    map = jsonObject
//                );
//                for (i in 0..(jsonObject.names() - 1)) {
//                    addressBook.map.put(jsonObject.)
//                }
//                data.add(addressBook)
//            }
//            for (i in 0..(contactData.names().length() - 1)) {
//
//                var jsonObject = contactData.getJSONObject(contactData.names().get(i).toString());
//
//                var addressBook: AddressBook = AddressBook(
//                    id = contactData.names().get(i).toString(),
//                    displayName = if (jsonObject.has("displayName")) jsonObject.getString("displayName") else "",
//                    emptyMap<String,String>(),
////                    mobile = if (jsonObject.has("phone")) jsonObject.getString("phone") else "",
////                    mobileId = if (jsonObject.has("id")) jsonObject.getString("id") else "",
//                    lavel = if (jsonObject.has("lavel")) jsonObject.getString("lavel") else "",
//                    map = jsonObject
//                );
//
//                data.add(addressBook)
//            }
//            emit(data)
//        }
//            .flowOn(Dispatchers.IO)
//            .onStart {
//                LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(true)
//            }
//            .onEmpty {
//                Logger.d("executeReqWithFlow:onEmpty")
//            }
//            .catch { exception ->
//                Logger.e("executeReqWithFlow:exception${exception.message}")
//            }
//            .onCompletion {
//                LiveDataBus.with(BusConstant.LOADING, Boolean::class.java).postValue(false)
//
//            }
//            .collect {
//                Logger.d("executeReqWithFlow: collect")
//            }
//        return data
    }

    fun getContactInfo(context: Context): MutableMap<String,AddressBook> {
        try {
            var contactData: MutableMap<String,AddressBook> = mutableMapOf();
            lateinit var jsonObject: AddressBook
//        list = ArrayList<ContactsContract.Contacts>()
            var mimetype = ""
            var oldrid = -1
            var contactId = -1
            // 1.查询通讯录所有联系人信息，通过id排序，我们看下android联系人的表就知道，所有的联系人的数据是由RAW_CONTACT_ID来索引开的
            // 所以，先获取所有的人的RAW_CONTACT_ID
            val cursor = MyApplication.context.contentResolver?.query(
                ContactsContract.Data.CONTENT_URI,
                null, null, null, ContactsContract.Contacts.Data.RAW_CONTACT_ID
            )
            if (cursor != null) {
                var numm = 0
                while (cursor.moveToNext()) {
                    contactId = cursor.getInt(
                        cursor
                            .getColumnIndex(ContactsContract.Contacts.Data.RAW_CONTACT_ID)
                    )
                    /*for(cn in cursor.columnNames){
                        var i = cursor.getColumnIndex(cn);
                        var c = cursor.getString(i);
                        if(c!=null&&c.equals("139703099951")){
                            mimetype = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE))
                            val phoneType: Int = cursor
                                .getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)) // 手机
                            val labelindex: Int =
                                cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL)
                            var phoneLabel = "";
                            if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM){
                                phoneLabel = cursor.getStringOrNull(labelindex).let { it.toString() }
                            }else{
                                phoneLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), phoneType, "").toString();
                            }
                            Logger.v("cn=$cn,index:$i,column:$c，mimetype：$mimetype,mimetype:$mimetype,phoneLabel:$phoneLabel")
                        }
                    }*/
                    if (oldrid != contactId) {
                        jsonObject = AddressBook(contactId,mutableMapOf())
                        contactData?.put(contactId.toString(), jsonObject)
                        numm++
                        oldrid = contactId
                    }
                    mimetype =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE)) // 取得mimetype类型,扩展的数据都在这个类型里面
    //                if (ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE == mimetype) {
    //                    val displayName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)
    //                    )
    //                    jsonObject.put("displayName", displayName)
    //                }

                    // 1.1,拿到联系人的各种名字
    //                if (ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE == mimetype) {
    //                    cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)
    //                    )
    //                    val prefix = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PREFIX)
    //                    )
    //                    jsonObject.put("prefix", prefix)
    //                    val firstName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)
    //                    )
    //                    jsonObject.put("firstName", firstName)
    //                    val middleName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME)
    //                    )
    //                    jsonObject.put("middleName", middleName)
    //                    val lastname = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)
    //                    )
    //                    jsonObject.put("lastname", lastname)
    //                    val suffix = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.SUFFIX)
    //                    )
    //                    jsonObject.put("suffix", suffix)
    //                    val phoneticFirstName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_FAMILY_NAME)
    //                    )
    //                    jsonObject.put("phoneticFirstName", phoneticFirstName)
    //                    val phoneticMiddleName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_MIDDLE_NAME)
    //                    )
    //                    jsonObject.put("phoneticMiddleName", phoneticMiddleName)
    //                    val phoneticLastName = cursor.getString(
    //                        cursor
    //                            .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_GIVEN_NAME)
    //                    )
    //                    jsonObject.put("phoneticLastName", phoneticLastName)
    //                }
                    // 1.2 获取各种电话信息
    //                if (ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE == mimetype) {
                        val phoneType: Int = cursor
                            .getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))
                        val labelindex: Int =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL)

                        var phoneLabel = "";
                        if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM){
                            phoneLabel = cursor.getStringOrNull(labelindex).let { it.toString() }
                        }else{
                            phoneLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), phoneType, "").toString();
                        }


                        var name = "mobileEmail";

                        // 住宅电话
    //                    when (phoneType) {
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE -> name = "mobile";
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_HOME -> name = "homeNum";
    //                        // 单位电话
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_WORK -> name = "jobNum";
    //                        // 单位传真
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK -> name = "workFax";
    //                        // 住宅传真
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME -> name =
    //                            "homeFax"; // 寻呼机
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_PAGER -> name = "pager";
    //                        // 回拨号码
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK -> name = "quickNum";
    //                        // 公司总机
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN -> name = "jobTel";
    //                        // 车载电话
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_CAR -> name = "carNum"; // ISDN
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_ISDN -> name = "isdn"; // 总机
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_MAIN -> name = "tel";
    //                        // 无线装置
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_RADIO -> name =
    //                            "wirelessDev"; // 电报
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_TELEX -> name = "telegram";
    //                        // TTY_TDD
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD -> name = "tty_tdd";
    //                        // 单位手机
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE -> name =
    //                            "jobMobile";
    //                        // 单位寻呼机
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER -> name =
    //                            "jobPager"; // 助理
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT -> name =
    //                            "assistantNum"; // 彩信
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_MMS -> name = "mms";
    //                        //自定义
    //                        ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM -> name = label;
    //                    }

                        name = getNames(phoneLabel, jsonObject);
                        var mobile: String = cursor.getStringOrNull(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).let { it.toString() }

                        mobile = mobile.replace("[", "")
                            .replace("]", "")
                            .replace(" ", "")
                            .replace("+86", "")
                            .replace("-", "")
                            .trim();
    //                    jsonObject.put(name, mobile)
                        val _id =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data._ID))

                        var m = Mobile(_id,phoneLabel,mobile)
                        jsonObject.mobiles.put(_id, m)

    //                }
                }


                cursor.close()
                return contactData
            }

            return contactData;
        } catch (e: Exception) {
            e.message?.let { Logger.e(it) };
            ToastUtil.show("更新出错，请联系管理员")
        }
        return mutableMapOf();
    }

    fun getNames(n: String, address: AddressBook): String {

        var name = n;
        if (address.mobiles.containsKey(name)) {
            var list = name.split("-");
            if (list.size > 1) {

                name = list[0] + "-" + (list[1].toInt() + 1);
            } else {
                name = name + "-1"
            }
            return getNames(name, address);
        }
        return name;
    }
}