package com.example.address_book_kt.ui.address


import com.example.address_book_kt.data.model.ImportPhone
import com.example.common.http.BaseResp
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface FirstApi {
    /**
     * 注册
     */
    @FormUrlEncoded
    @POST("/sp/sp/spPhoneImport/searchShort")
    suspend fun searchShort( @Field("myPhone") myPhone: String, @Field("longPhone") longPhone: String) : BaseResp<MutableList<ImportPhone>>

}