package com.example.address_book_kt.ui.address

import android.content.ContentProviderOperation
import android.content.ContentValues
import android.content.Context
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.StructuredName
import android.widget.Toast
import androidx.lifecycle.*
import com.carman.kotlin.coroutine.request.repository.MainRepository
import com.example.address_book_kt.MyApplication
import com.example.address_book_kt.data.model.ImportPhone
import com.example.address_book_kt.module.AddressBook
import com.example.address_book_kt.ui.login.repo.LoginRepo
import com.example.common.base.BaseViewModel
import com.example.common.bus.BusConstant
import com.example.common.bus.LiveDataBus
import com.fuusy.common.network.net.StateLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject


@HiltViewModel
class FirstViewModel @Inject constructor(private val repository: FirstRepository): BaseViewModel() {

//    var addressBooks: MutableLiveData<MutableList<AddressBook>> = mutableListOf<AddressBook>();

    private val _addressBooks =
        MutableLiveData<MutableMap<String,AddressBook>>(mutableMapOf<String,AddressBook>())
    val addressBooksState: LiveData<MutableMap<String,AddressBook>> = _addressBooks


    var searchData = StateLiveData<MutableList<ImportPhone>>()

    private val _result = MutableLiveData<String>()
    val result: LiveData<String> = _result

    fun getTXL(context: Context){
        viewModelScope.launch{
            _addressBooks.value = repository.getTXL(context)
        }
    }



    fun searchData(){
        viewModelScope.launch{
//            repository.searchData(_addressBooks.value!!,searchData)
        }

    }

    fun updateDate(context: Context){
        viewModelScope.launch{
            repository.updateTXL(_addressBooks.value!!,searchData,_result)
            getTXL(context)
        }

    }



    /**
     * 弹Toast
     */
    private fun showToast(msg: String) {
        LiveDataBus.with(BusConstant.ERROR, String::class.java).postValue(msg)
    }


}