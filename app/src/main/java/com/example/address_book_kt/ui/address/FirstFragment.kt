package com.example.address_book_kt.ui.address

import android.Manifest
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.address_book_kt.adapter.FirstAdapter
import com.example.address_book_kt.data.model.ImportPhone
import com.example.common.base.BaseFragment
import com.example.address_book_kt.databinding.FragmentFirstBinding
import com.example.address_book_kt.ui.login.LoginViewModel
import com.example.common.http.BaseResp
import com.fuusy.common.network.net.IStateObserver
import com.permissionx.guolindev.PermissionX
import dagger.hilt.android.AndroidEntryPoint


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class FirstFragment : BaseFragment<FragmentFirstBinding>()  {


    val viewModel: FirstViewModel by viewModels()

//    val contacts_columns = arrayOf("display_name", "sort_key", "contact_id", "data1")

//    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
//    private val binding get() = _binding!!

//    val viewModel: FirstViewModel = ViewModelProvider(this).get(FirstViewModel::class.java)
    //懒加载方式，第一次使用
//    val viewModel: FirstViewModel by viewModels()
    //这种方式也可以 在Fragment中通过activityViewModels创建的ViewModel，得到的将会这个Fragment所在的Acivity的ViewModel
//    val viewModel:FirstViewModel by activityViewModels()

    lateinit var firstAdapter :FirstAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState);
//        viewModel.contactData
//        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun FragmentFirstBinding.initBinding() {

        firstAdapter = FirstAdapter()
        with(recyclerView){
            layoutManager = LinearLayoutManager(activity).apply {
                orientation = RecyclerView.VERTICAL
            }
            adapter = firstAdapter
        }
//        viewModel.addressBooksState.observe(this@FirstFragment,{
//            firstAdapter.setData(viewModel.addressBooksState.value)
//            firstAdapter.notifyDataSetChanged()
//        });
        viewModel.searchData.observe(this@FirstFragment,object : IStateObserver<MutableList<ImportPhone>>(null) {
            override fun onDataChange(data: MutableList<ImportPhone>?) {

            }

            override fun onChanged(t: BaseResp<MutableList<ImportPhone>>) {
                super.onChanged(t)
                activity?.let { it1 -> viewModel.getTXL(it1.applicationContext) }
            }

            override fun onDataEmpty() {
                super.onDataEmpty()
                showToast("无匹配号码")
            }

            override fun onError(e: Throwable?) {
                super.onError(e)

            }

            override fun onReload(v: View?) {

            }
        })



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.buttonFirst.setOnClickListener {


            PermissionX.init(activity)
                .permissions(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)
                .onExplainRequestReason { scope, deniedList ->
                    scope.showRequestReasonDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .onForwardToSettings { scope, deniedList ->
                    scope.showForwardToSettingsDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .request { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        activity?.let { it1 -> viewModel.getTXL(it1.applicationContext) }
                    } else {
                        Toast.makeText(activity, "请开启通讯录读写权限", Toast.LENGTH_LONG).show()
                    }
                }
        }

        mBinding.buttonUpdate.setOnClickListener {
            PermissionX.init(activity)
                .permissions(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)
                .onExplainRequestReason { scope, deniedList ->
                    scope.showRequestReasonDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .onForwardToSettings { scope, deniedList ->
                    scope.showForwardToSettingsDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .request { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        viewModel.searchData()
                    } else {
                        Toast.makeText(activity, "请开启通讯录读写权限", Toast.LENGTH_LONG).show()
                    }
                }
        }
    }

    fun initList(){
    }



    private val PROJECTION: Array<out String> = arrayOf(
        ContactsContract.Data._ID,
        ContactsContract.Data.MIMETYPE,
        ContactsContract.Data.DATA1,
        ContactsContract.Data.DATA2,
        ContactsContract.Data.DATA3,
        ContactsContract.Data.DATA4,
        ContactsContract.Data.DATA5,
        ContactsContract.Data.DATA6,
        ContactsContract.Data.DATA7,
        ContactsContract.Data.DATA8,
        ContactsContract.Data.DATA9,
        ContactsContract.Data.DATA10,
        ContactsContract.Data.DATA11,
        ContactsContract.Data.DATA12,
        ContactsContract.Data.DATA13,
        ContactsContract.Data.DATA14,
        ContactsContract.Data.DATA15
    )



    override fun onDestroyView() {
        super.onDestroyView()
    }


//    inner class Adapter :RecyclerView.Adapter<Adapter.MyViewHolder>(){
//
//        inner class MyViewHolder(mBinding:ItemAddressBookBinding):RecyclerView.ViewHolder(mBinding.root)
//
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//            val mBinding = DataBindingUtil.inflate<ItemAddressBookBinding>(LayoutInflater.from(parent.context), R.layout.item_address_book
//                ,parent, false)
//            return MyViewHolder(mBinding)
//        }
//
//        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        }
//
//        override fun getItemCount(): Int {
//            return 0
//        }
//    }




}