package com.example.address_book_kt.ui.login.repo


import com.example.address_book_kt.data.model.AppVersion
import com.example.common.http.BaseResp
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginApi {
    /**
     * 注册
     */
    @FormUrlEncoded
    @POST("user/register")
    suspend fun register(@Field("username") username: String,@Field("password") password: String
                         ,@Field("repassword") repassword: String) : BaseResp<String>

    /**
     * 登录
     */
    @FormUrlEncoded
    @POST("/sp/system/user/login")
    suspend fun login(@Field("phone") username: String,@Field("password") password: String): BaseResp<String>

    /**
     * 检查版本号
     */
    @FormUrlEncoded
    @POST("/sp/sp/version/searchVersion")
    suspend fun checkVersion(@Field("versionCode") versionCode: String,@Field("versionName") versionName: String): BaseResp<AppVersion>
}