package com.example.address_book_kt.ui.login

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.example.address_book_kt.data.LoginRepository
import com.example.address_book_kt.data.Result

import com.example.address_book_kt.R
import com.example.address_book_kt.data.model.AppVersion
import com.example.address_book_kt.data.model.ImportPhone
import com.example.address_book_kt.module.AddressBook
import com.example.address_book_kt.ui.address.FirstRepository
import com.example.address_book_kt.ui.login.repo.LoginRepo
import com.example.common.base.BaseViewModel
import com.fuusy.common.network.net.StateLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val loginRepository: LoginRepo,private val firstRepository: FirstRepository) : BaseViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val _addressBooks =
        MutableLiveData<MutableMap<String,AddressBook>>(mutableMapOf<String,AddressBook>())
    val addressBooksState: LiveData<MutableMap<String,AddressBook>> = _addressBooks


    var searchData = StateLiveData<MutableList<ImportPhone>>()


    val appVersionData = StateLiveData<AppVersion>()

    val loginLiveData = StateLiveData<String>()
    val registerLiveData = StateLiveData<String>()


    private val _result = MutableLiveData<String>()
    val result: LiveData<String> = _result

    fun login(username: String, password: String) {

        viewModelScope.launch {
            loginRepository.login(username, password, loginLiveData)
        }


//        // can be launched in a separate asynchronous job
//        val result = loginRepository.login(username, password)
//
//        if (result is Result.Success) {
//            _loginResult.value =
//                LoginResult(success = LoggedInUserView(displayName = result.data.displayName))
//        } else {
//            _loginResult.value = LoginResult(error = R.string.login_failed)
//        }

    }

    fun checkVersion(code: String, name: String) {

        viewModelScope.launch {
            loginRepository.checkVersion(code, name, appVersionData)
        }


//        // can be launched in a separate asynchronous job
//        val result = loginRepository.login(username, password)
//
//        if (result is Result.Success) {
//            _loginResult.value =
//                LoginResult(success = LoggedInUserView(displayName = result.data.displayName))
//        } else {
//            _loginResult.value = LoginResult(error = R.string.login_failed)
//        }

    }

    fun loginDataChanged(username: String, check: Boolean) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!check) {
            _loginForm.value = LoginFormState(passwordError = R.string.write_and_read)
        }  else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }


    fun getTXL(context: Context) {
        viewModelScope.launch {
            _addressBooks.value = firstRepository.getTXL(context)
        }
    }


    fun searchData(myPhone:String) {
        viewModelScope.launch {
            firstRepository.searchData(myPhone,_addressBooks.value!!, searchData)
        }

    }

    fun updateDate() {
        viewModelScope.launch {
            firstRepository.updateTXL(_addressBooks.value!!, searchData,_result)
        }

    }

    fun writeShortPhone(myPhone:String,context: Context){
        viewModelScope.launch {
            _addressBooks.value = firstRepository.getTXL(context)
            firstRepository.searchData(myPhone,_addressBooks.value!!, searchData)
//            firstRepository.updateTXL(_addressBooks.value!!, searchData)
        }

    }
}