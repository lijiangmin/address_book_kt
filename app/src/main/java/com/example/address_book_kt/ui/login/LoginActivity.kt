package com.example.address_book_kt.ui.login

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.carman.kotlin.coroutine.base.BaseActivity
import com.carman.kotlin.coroutine.base.BaseVMActivity
import com.example.address_book_kt.BuildConfig
import com.example.address_book_kt.MainActivity
import com.example.address_book_kt.MyApplication
import com.example.address_book_kt.databinding.ActivityLoginBinding

import com.example.address_book_kt.R
import com.example.address_book_kt.data.LoginRepository
import com.example.address_book_kt.data.model.AppVersion
import com.example.address_book_kt.data.model.ImportPhone
import com.example.address_book_kt.module.AddressBook
import com.example.common.base.BaseDialogFragment
import com.example.common.base.BaseViewModel
import com.example.common.base.OnChildClickListener
import com.example.common.http.BaseRepository
import com.example.common.http.BaseResp
import com.fuusy.common.network.net.IStateObserver
import com.kelin.apkUpdater.ApkUpdater
import com.kelin.apkUpdater.SignatureType
import com.kelin.apkUpdater.UpdateInfoImpl
import com.kelin.apkUpdater.UpdateType
import com.orhanobut.logger.Logger
import com.permissionx.guolindev.PermissionX
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {


    val viewModel: LoginViewModel by viewModels()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        viewModel.checkVersion(BuildConfig.VERSION_CODE.toString(),BuildConfig.VERSION_NAME);
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }



    override fun initObserve() {
        val username = mBinding.username
        val password = mBinding.password
        val login = mBinding.login
        val loading = mBinding.loading



        viewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
//            if (loginState.passwordError != null) {
//                password.error = getString(loginState.passwordError)
//            }
        })

        /*viewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            finish()
        })*/

        viewModel.result.observe(this@LoginActivity, Observer {

            resulttv.text = it;
        })

        viewModel.loginLiveData.observe(this, object : IStateObserver<String>(null) {
            override fun onDataChange(data: String?) {
                showToast("查询成功")
                if(data==null){
                    showToast("查询失败，找不到用户或账号密码错误")
                    return
                }else{
                    MyApplication.token = data
                    MainActivity.start(this@LoginActivity)
                }
            }

            override fun onChanged(t: BaseResp<String>) {
                super.onChanged(t)
            }

            override fun onDataEmpty() {
                super.onDataEmpty()
                showToast("无数据")
            }

            override fun onError(e: Throwable?) {
                super.onError(e)

            }

            override fun onReload(v: View?) {

            }
        })

        username.afterTextChanged {
            viewModel.loginDataChanged(
                username.text.toString(),
                aggre.isChecked
            )
        }

        aggre.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel.loginDataChanged(
                username.text.toString(),
                aggre.isChecked
            )
        }






        aggreText.setOnClickListener { l->
            BaseDialogFragment.
            newInstance().
            setWidthPercentSize(0.8f).
            setHeightPercentSize(0.8f).
            setContentView(R.layout.dialog_xieyi_yinsi_style).
            show(this)
                .setOnChildClickListener(object : OnChildClickListener {
                    override fun onChildViewClick(dialog: BaseDialogFragment?, v: View?) {
                        dialog?.dismiss();
                    }
                },R.id.tv_cancle);
        }

//        password.apply {
//            afterTextChanged {
//                viewModel.loginDataChanged(
//                    username.text.toString(),
//                    password.text.toString()
//                )
//            }
//
//            setOnEditorActionListener { _, actionId, _ ->
//                when (actionId) {
//                    EditorInfo.IME_ACTION_DONE ->
//                        viewModel.login(
//                            username.text.toString(),
//                            password.text.toString()
//                        )
//                }
//                false
//            }
//
//            login.setOnClickListener {
////                loading.visibility = View.VISIBLE
//                viewModel.login(username.text.toString(), password.text.toString())
//            }
//        }


        login.setOnClickListener {
            PermissionX.init(this)
                .permissions(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)
                .onExplainRequestReason { scope, deniedList ->
                    scope.showRequestReasonDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .onForwardToSettings { scope, deniedList ->
                    scope.showForwardToSettingsDialog(deniedList, "请开启通讯录读写权限", "OK", "Cancel")
                }
                .request { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        viewModel.writeShortPhone(username.text.toString(),applicationContext)
                    } else {
                        Toast.makeText(this, "请开启通讯录读写权限", Toast.LENGTH_LONG).show()
                    }
                }
        }
//        viewModel.addressBooksState.observe(this,Observer{
//            viewModel.searchData(username.text.toString())
//        })
//
        viewModel.searchData.observe(this , object : IStateObserver<MutableList<ImportPhone>>(null) {


            override fun onDataChange(data: MutableList<ImportPhone>?) {
                super.onDataChange(data)
            }
            override fun onChanged(t: BaseResp<MutableList<ImportPhone>>) {
                super.onChanged(t)
                viewModel.updateDate()
            }

            override fun onDataEmpty() {
                super.onDataEmpty()
            }

            override fun onError(e: Throwable?) {
                super.onError(e)
            }

            override fun onReload(v: View?) {
            }
        })

        viewModel.appVersionData.observe(this, object : IStateObserver<AppVersion>(null) {
            override fun onDataChange(data: AppVersion?) {

            }

            override fun onChanged(t: BaseResp<AppVersion>) {
                super.onChanged(t)
                t.data?.let{

                    ApkUpdater.Builder().create().check(
                        UpdateInfoImpl(
                            it.downLoadsUrl, //安装包下载地址
                            it.versionCode?:0, //网络上的版本号，用于判断是否可以更新(是否大于本地版本号)。
                            it.versionName, //版本名称，用于显示在弹窗中，以告知用户将要更到哪个版本。
                            when (it.updateType) {
                                "强" -> UpdateType.UPDATE_FORCE
                                "正常" -> UpdateType.UPDATE_NORMAL
                                else -> { // 注意这个块
                                    UpdateType.UPDATE_WEAK
                                }
                            },  //是否是强制更新，如果干参数为true则用户没有进行更新就不能继续使用App。(当旧版本存在严重的Bug时或新功能不与旧版兼容时使用)
                            it.updateMessageTitle,  //升级弹窗的标题。
                            it.updateMessage, //升级弹窗的消息内容，用于告知用户本次更新的内容。
                            null, //安装包完整性校验开启，并使用MD5进行校验，如果不想开启，传null。(目前只支持MD5和SHA1)
                            it.signature  //完成性校验的具体值，返回空或null则不会进行校验。
                        )
                    )
                }

            }
            override fun onDataEmpty() {
                super.onDataEmpty()
            }

            override fun onError(e: Throwable?) {
                super.onError(e)

            }

            override fun onReload(v: View?) {

            }
        })

    }

    override fun ActivityLoginBinding.initBinding() {

    }



}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
