package com.example.address_book_kt

import android.app.Application
import android.content.Context
import com.kelin.apkUpdater.ApkUpdater
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class MyApplication : Application() {

    companion object{
        lateinit var context: Context
        var token: String? = null

    }

    override fun onCreate() {
        super.onCreate()
        context = this;
        Logger.addLogAdapter(object : AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
        ApkUpdater.init(this, "$packageName.fileProvider")

    }
}