package com.example.address_book_kt.data.model


import com.google.gson.annotations.SerializedName

data class ImportPhone(
    @SerializedName("createDate")
    val createDate: String?,
    @SerializedName("createOper")
    val createOper: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("longPhone")
    val longPhone: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("shortPhone")
    val shortPhone: String?,
    @SerializedName("status")
    val status: Int?
)