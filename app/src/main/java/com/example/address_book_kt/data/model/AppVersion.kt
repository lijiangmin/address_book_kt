package com.example.address_book_kt.data.model


import com.google.gson.annotations.SerializedName

data class AppVersion(
    @SerializedName("createDate")
    val createDate: String?,
    @SerializedName("downLoadsUrl")
    val downLoadsUrl: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("signature")
    val signature: String?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("updateMessage")
    val updateMessage: String?,
    @SerializedName("updateMessageTitle")
    val updateMessageTitle: String?,
    @SerializedName("updateType")
    val updateType: String?,
    @SerializedName("versionCode")
    val versionCode: Int?,
    @SerializedName("versionName")
    val versionName: String?
)