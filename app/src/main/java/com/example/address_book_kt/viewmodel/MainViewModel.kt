package com.example.address_book_kt.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.carman.kotlin.coroutine.request.repository.MainRepository
import com.example.address_book_kt.R
import com.example.address_book_kt.ui.login.repo.LoginRepo
import com.example.common.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(    private val repository: MainRepository):BaseViewModel() {
}