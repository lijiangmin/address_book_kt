package com.example.address_book_kt

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import com.carman.kotlin.coroutine.base.BaseActivity
import com.carman.kotlin.coroutine.base.BaseVMActivity
import com.example.address_book_kt.databinding.ActivityMainBinding
import com.example.address_book_kt.ui.login.LoginViewModel
import com.example.address_book_kt.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.reflect.KProperty

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    val viewModel: MainViewModel by viewModels()


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun ActivityMainBinding.initBinding() {
    }

    override fun initObserve() {

    }

    companion object{
        fun start(context: Context) {
            context.startActivity(Intent(context,MainActivity::class.java))
        }

    }
}

