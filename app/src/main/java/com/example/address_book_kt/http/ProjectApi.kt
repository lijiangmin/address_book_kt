package com.example.address_book_kt.http

import com.example.common.http.BaseResp
import retrofit2.http.Field
import retrofit2.http.POST

interface ProjectApi {
    @POST("/sp/system/user/login")
    suspend fun loadProjectTree(@Field("phone") phone: String): BaseResp<String>
}